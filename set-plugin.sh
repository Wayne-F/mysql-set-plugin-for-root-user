#!/bin/bash


# Set plugin for root user to 'mysql_native_password'

# Check mysql server status
echo '*************************'
echo 'Check mysql server status'
echo '*************************'
sudo systemctl status mysql --no-pager

echo '*************************************************************'
echo 'Updating plugin for root user to user "mysql_native_password"'
echo '*************************************************************'
echo '*************************************************************'

# Pass --password=xxxx to set password

echo '**************************'
echo 'Original plugin table data'
echo '**************************'
mysql --user=root -e "SELECT User, Host, plugin FROM mysql.user;"

echo '*************************'
echo 'Attempting to update plugin for root user'
echo '*************************'
mysql --user=root -e "USE mysql; UPDATE user SET plugin='mysql_native_password' WHERE User='root';"

echo '*************************'
echo 'Plugin table after update'
echo '*************************'
mysql --user=root -e "SELECT User, Host, plugin FROM mysql.user;"

mysql --user=root -e "FLUSH PRIVILEGES;"

sudo systemctl restart mysql

echo '*************************'
echo 'Check mysql server status'
echo '*************************'
sudo systemctl status mysql --no-pager

echo '****************************************************************************'
echo 'SUCCESS - plugin for root mysql user updated to user "mysql_native_password"'
echo '****************************************************************************'
